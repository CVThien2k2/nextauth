import { LoginForm } from "./form";

export default function LoginPage() {
  return (
    <div
      style={{
        display: "flex",
        height: "70vh",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div>
        <LoginForm />
      </div>
    </div>
  );
}
