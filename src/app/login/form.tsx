"use client";
import {
  TextInput,
  PasswordInput,
  Checkbox,
  Anchor,
  Paper,
  Title,
  Text,
  Container,
  Group,
  Button,
} from "@mantine/core";
import Link from "next/link";
import { signIn } from "next-auth/react";
import { ChangeEvent, useState } from "react";
import classes from "./login.module.css";
export const LoginForm = () => {
  let [loading, setLoading] = useState(false);
  let [formValues, setFormValues] = useState({
    email: "",
    password: "",
    callbackUrl: "/",
  });

  const [error, setError] = useState("");

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setLoading(true);
    try {
      const res = await signIn("credentials", {
        email: formValues.email,
        password: formValues.password,
        redirect: false,
      });
      setLoading(false);
      if (res?.error) {
        setError("Invalid email or password...");
        setTimeout(() => {
          setError("");
        }, 5000);
      } else {
        window.location.href = "/";
      }
    } catch (err: any) {}
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormValues({ ...formValues, [name]: value });
  };

  return (
    <Container size={420} my={40}>
      <Text c="dimmed" size="sm" ta="center" mt={5}>
        Do not have an account yet?{" "}
        <Link href="/register" style={{ marginRight: 10 }}>
          <Anchor size="sm" component="button">
            Create account?
          </Anchor>
        </Link>
      </Text>

      <form onSubmit={onSubmit}>
        <Paper withBorder shadow="md" p={30} mt={30} radius="md">
          <TextInput
            label="Email"
            placeholder="exmample@gmail.com"
            required
            value={formValues.email}
            onChange={handleChange}
            name="email"
          />
          <PasswordInput
            label="Password"
            placeholder="Your password"
            required
            mt="md"
            value={formValues.password}
            onChange={handleChange}
            name="password"
          />
          {error && (
            <div className={classes.error}>
              <Text c="red" size="sm" ta="center">
                {error}
              </Text>
            </div>
          )}
          <Group justify="space-between" mt="lg">
            <Checkbox label="Remember me" />
            <Anchor component="button" size="sm">
              Forgot password?
            </Anchor>
          </Group>
          <Button type="submit" fullWidth mt="xl">
            {!loading ? "Sign in" : "Loading.."}
          </Button>
        </Paper>
      </form>
    </Container>
  );
};
