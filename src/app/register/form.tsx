"use client";
import {
  TextInput,
  PasswordInput,
  Checkbox,
  Anchor,
  Paper,
  Title,
  Text,
  Container,
  Group,
  Button,
} from "@mantine/core";
import Link from "next/link";
import classes from "@/app/login/login.module.css";
import { signIn } from "next-auth/react";
import { ChangeEvent, useState } from "react";

export const RegisterForm = () => {
  let [loading, setLoading] = useState(false);
  let [formValues, setFormValues] = useState({
    name: "",
    email: "",
    password: "",
  });

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setLoading(true);

    try {
      const res = await fetch("/api/register", {
        method: "POST",
        body: JSON.stringify(formValues),
        headers: {
          "Content-Type": "application/json",
        },
      });

      setLoading(false);
      if (!res.ok) {
        alert((await res.json()).message);
        return;
      }

      signIn("credentials", {
        email: formValues.email,
        password: formValues.password,
        callbackUrl: "/",
      });
    } catch (error: any) {
      setLoading(false);
      console.error(error);
      alert(error.message);
    }
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormValues({ ...formValues, [name]: value });
  };

  return (
    <Container size={420} my={40}>
      <Text c="dimmed" size="sm" ta="center" mt={5}>
        Do not have an account yet?{" "}
        <Link href="/login" style={{ marginRight: 10 }}>
          <Anchor size="sm" component="button">
            Login?
          </Anchor>
        </Link>
      </Text>

      <form onSubmit={onSubmit}>
        <Paper withBorder shadow="md" p={30} mt={30} radius="md">
          <TextInput
            label="Name"
            placeholder="Your Name"
            required
            value={formValues.name}
            onChange={handleChange}
            name="name"
          />
          <TextInput
            label="Email"
            placeholder="exmample@gmail.com"
            required
            value={formValues.email}
            onChange={handleChange}
            name="email"
          />
          <PasswordInput
            label="Password"
            placeholder="Your password"
            required
            mt="md"
            value={formValues.password}
            onChange={handleChange}
            name="password"
          />

          <Group justify="space-between" mt="lg">
            <Checkbox label="Remember me" />
            <Anchor component="button" size="sm">
              Forgot password?
            </Anchor>
          </Group>

          <Button type="submit" fullWidth mt="xl">
            {!loading ? "Register" : "Loading..."}
          </Button>
        </Paper>
      </form>
    </Container>
  );
};
